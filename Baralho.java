import java.util.ArrayList;

public class Baralho{
	private boolean atributo1MaiorMelhor;
	private boolean atributo2MaiorMelhor;
	private boolean atributo3MaiorMelhor;
	private boolean atributo4MaiorMelhor;
	private boolean atributo5MaiorMelhor;
	private ArrayList<Carta> cartas;
	private String[] atributoNomes;

	public Baralho(
		String a1, String a2, String a3, String a4, String a5,
		boolean b1, boolean b2, boolean b3, boolean b4, boolean b5){

		atributoNomes = new String[5];
		atributoNomes[0] = a1;
		atributoNomes[1] = a2;
		atributoNomes[2] = a3;
		atributoNomes[3] = a4;
		atributoNomes[4] = a5;

		this.atributo1MaiorMelhor = b1;
		this.atributo2MaiorMelhor = b2;
		this.atributo3MaiorMelhor = b3;
		this.atributo4MaiorMelhor = b4;
		this.atributo5MaiorMelhor = b5;
		
		this.cartas = new ArrayList<Carta>();

	}

	public String pegarNomeDeAtributoPorIndice(int i){
		return this.atributoNomes[i-1];
	}

	private Carta verificarSeSuperTrunfoGanhaDireto(Carta c1, Carta c2){
		if(c1.ehSuperTrunfo() && c2.getTipo() != 'A'){
			return c1;
		}
		else if(c2.ehSuperTrunfo() && c1.getTipo() != 'A'){
			return c2;
		}
		return null;
	}

	public Carta cartaGanhadora(Carta c1, Carta c2, int atributo){
		
		Carta superTrunfo = this.verificarSeSuperTrunfoGanhaDireto(c1,c2);
		if(superTrunfo != null){
			return superTrunfo;
		}

		int valor = 0;
		boolean maiorMelhor = false;
		switch(atributo){
			case 1:
				valor = c1.getAtributo1()-c2.getAtributo1();
				maiorMelhor = this.atributo1MaiorMelhor;
			break;
			case 2:
				valor = c1.getAtributo2()-c2.getAtributo2();
				maiorMelhor = this.atributo2MaiorMelhor;
			break;
			case 3:
				valor = c1.getAtributo3()-c2.getAtributo3();
				maiorMelhor = this.atributo3MaiorMelhor;
			break;
			case 4:
				valor = c1.getAtributo4()-c2.getAtributo4();
				maiorMelhor = this.atributo4MaiorMelhor;
			break;
			case 5:
				valor = c1.getAtributo5()-c2.getAtributo5();
				maiorMelhor = this.atributo5MaiorMelhor;
			break;
		}
		if(valor == 0){
			return null;
		}
		else if(valor > 0){
			if(maiorMelhor){
				return c1;
			}else{
				return c2;
			}
		}else{
			if(maiorMelhor){
				return c2;
			}else{
				return c1;
			}
		}
	}

	public void adicionarCartas(ArrayList<Carta> cartas){
		this.cartas = cartas;
	}

	public ArrayList<Carta> pegarCartas(){
		return this.cartas;
	}

	public boolean atributo1MaiorEhMelhor(){
		return this.atributo1MaiorMelhor;
	}
	public boolean atributo2MaiorEhMelhor(){
		return this.atributo2MaiorMelhor;
	}
	public boolean atributo3MaiorEhMelhor(){
		return this.atributo3MaiorMelhor;
	}
	public boolean atributo4MaiorEhMelhor(){
		return this.atributo4MaiorMelhor;
	}
	public boolean atributo5MaiorEhMelhor(){
		return this.atributo5MaiorMelhor;
	}

}