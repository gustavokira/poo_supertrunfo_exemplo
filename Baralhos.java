import java.util.ArrayList;

class Baralhos{
	
	public static Baralho criarBaralhoDeuses(){
		Baralho b = new Baralho(
			"poder","forca","inteligencia","velocidade","adoradores",
			true,true, true, true, true
		);
		
		ArrayList<Carta> cartas = new ArrayList<Carta>();
		cartas.add(new Carta("Odin",		60, 70, 55, 30, 70, 'A', 1, false));
		cartas.add(new Carta("Thor",		70, 80, 30, 40, 80, 'A', 2, false));
       	cartas.add(new Carta("Loki",		80, 30, 75, 70, 40, 'A', 3, false));
        cartas.add(new Carta("Hela",		90, 80, 75, 40, 10, 'A', 4, false));
        cartas.add(new Carta("Fenrir",		70, 99, 15, 70, 10, 'A', 5, false));
        cartas.add(new Carta("Heimdallr",	50, 60, 65, 30, 40, 'A', 6, false));
        cartas.add(new Carta("Ullr",		50, 60, 55, 70, 40, 'A', 7, false));
        cartas.add(new Carta("Sif",			40, 40, 55, 50, 40, 'A', 8, false));

        cartas.add(new Carta("Zeus",	70, 50, 60, 30, 90,'B',1,true));
        cartas.add(new Carta("Hera",	40, 40, 65, 40, 60,'B',2,false));
        cartas.add(new Carta("Ares",	50, 80, 25, 60, 65,'B',3,false));
        cartas.add(new Carta("Poseidon",90, 50, 55, 40, 60,'B',4,false));
        cartas.add(new Carta("Hades",	70, 40, 55, 30, 30,'B',5,false));
        cartas.add(new Carta("Afrodite",50, 30, 30, 30, 99,'B',6,false));
        cartas.add(new Carta("Hermes",	40, 40, 50, 99, 50,'B',7,false));
        cartas.add(new Carta("Atena",	60, 50, 99, 40, 70,'B',8,false));
        

       	b.adicionarCartas(cartas);
       	
       	return b;
	}
}