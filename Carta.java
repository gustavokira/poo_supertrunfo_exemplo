public class Carta{
	
	private String nome;

	private int atributo1;
	private int atributo2;
	private int atributo3;
	private int atributo4;
	private int atributo5;
	
	private boolean supertrunfo;
	private char tipo;
	private int numero;

	public Carta( String nome,
				  int a1,
				  int a2,
				  int a3,
				  int a4,
				  int a5,
				  char t, int n, boolean st
				){

		this.nome = nome;

		this.atributo1 = a1;
		this.atributo2 = a2;
		this.atributo3 = a3;
		this.atributo4 = a4;
		this.atributo5 = a5;
		this.tipo = t;
		this.numero = n;
		this.supertrunfo = st;
	}

	public int getAtributo1(){
		return this.atributo1;
	}
	
	public int getAtributo2(){
		return this.atributo2;
	}
	public int getAtributo3(){
		return this.atributo3;
	}
	
	public int getAtributo4(){
		return this.atributo4;
	}
	public int getAtributo5(){
		return this.atributo5;
	}
	public int getNumero(){
		return this.numero;
	}
	public char getTipo(){
		return this.tipo;
	}
	public String getNome(){
		return this.nome;
	}
	public boolean ehSuperTrunfo(){
		return this.supertrunfo;
	}
}