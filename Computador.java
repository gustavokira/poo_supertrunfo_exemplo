import java.util.ArrayList;
import java.util.Random;

public class Computador{
	public static final int FACIL = 0;
	public static final int MEDIO = 1;
	private ArrayList<Carta> cartas;
	private Carta escolhida;
	private int dificuldade;

	public Computador(int dificuldade){
		this.cartas = new ArrayList<Carta>();
		this.dificuldade = dificuldade;
	}

	public void adicionarCarta(Carta c){
		this.cartas.add(c);
	}

	public int quantidadeDeCartas(){
		return this.cartas.size();
	}

	public void escolherCarta(){
		this.escolhida = this.cartas.remove(0);
	}

	public Carta verCartaEscolhida(){
		return this.escolhida;
	}

	public Carta pegarCartaEscolhida(){
		Carta c = this.escolhida;
		this.escolhida = null;
		return c;
	}

	public int escolherAtributo(){
		switch(this.dificuldade){
			case FACIL:
				return escolherPrimeiroAtributo();
			case MEDIO:
				return escolherAtributoAleatorio();
		}
		return -1;
	}

	private int escolherPrimeiroAtributo(){
		return 1;
	}

	private int escolherAtributoAleatorio(){
		Random random = new Random();
		int x = random.nextInt(5) + 1;
		return x;
	}

	private int escolherMelhorAtributo(){
		return 0;
	}
}