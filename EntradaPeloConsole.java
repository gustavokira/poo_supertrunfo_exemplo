import java.util.Scanner;
public class EntradaPeloConsole{
	
	private Scanner scanner;
		
	public EntradaPeloConsole(){
		this.scanner = new Scanner(System.in);
	}

	public int getOpcao(){
		int i = this.scanner.nextInt();
		return i;
	}

	public void esperarUmaTeclaQualquer(){
		this.scanner = new Scanner(System.in);
		String s = this.scanner.nextLine();
	}
}