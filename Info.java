public class Info{
	private Jogador jogador;
	private Computador computador;
	private Carta cartaJogador;
	private Carta cartaComputador;
	private String nomeAtributoEscolhido;
	private int valorAtributoEscolhido;
	private boolean vezDoJogador;
	private int quantidadeDeCartasNaPilha;
	private int resultado;
	private Baralho baralho;

	public Info(
		Jogador j, 
		Computador c, 
		Carta cj, 
		Carta cc, 
		int v, 
		String n,
		boolean vez,
		int quantidadeDeCartasNaPilha,
		Baralho baralho
	){
		this.jogador = j;
		this.computador = c;
		this.cartaJogador = cj;
		this.cartaComputador = cc;
		this.valorAtributoEscolhido = v;
		this.nomeAtributoEscolhido = n;
		this.vezDoJogador = vez;
		this.quantidadeDeCartasNaPilha = quantidadeDeCartasNaPilha;
		this.baralho = baralho;
	}
	public void empate(){
		this.resultado = 0;
	}
	public void jogadorGanhou(){
		this.resultado = 1;
	}

	public void computadorGanhou(){
		this.resultado = 2;
	}

	public String ganhador(){
		switch(this.resultado){
			case 0:
				return "empate";
			case 1:
				return "jogador";
			case 2:
				return "computador";
		}
		return null;
	}

	public int quantidadeDeCartasJogador(){
		return this.jogador.quantidadeDeCartas();
	}

	public int quantidadeDeCartasComputador(){
		return this.computador.quantidadeDeCartas();
	}
	
	public String getNomeAtributoEscolhido(){
		return this.nomeAtributoEscolhido;
	}

	public String getCartaJogadorNome(){
		return this.cartaJogador.getNome();
	}

	public String getCartaComputadorNome(){
		return this.cartaComputador.getNome();
	}

	public Carta getCartaJogador(){
		return this.cartaJogador;
	}

	public Carta getCartaComputador(){
		return this.cartaComputador;
	}

	public Baralho getBaralho(){
		return this.baralho;
	}

	public boolean vezDoJogador(){
		return this.vezDoJogador;
	}

	public int quantidadeDeCartasNaPilha(){
		return this.quantidadeDeCartasNaPilha;
	}
}
