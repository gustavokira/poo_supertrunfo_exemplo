import java.util.ArrayList;

public class Jogador{
	private ArrayList<Carta> cartas;
	private Carta escolhida;
	private int atributoEscolhido;
	
	public Jogador(){
		this.cartas = new ArrayList<Carta>();
	}

	public void adicionarCarta(Carta c){
		this.cartas.add(c);
	}

	public int quantidadeDeCartas(){
		return this.cartas.size();
	}

	public void escolherCarta(){
		this.escolhida = this.cartas.remove(0);
	}

	public Carta verCartaEscolhida(){
		return this.escolhida;
	}
	public Carta pegarCartaEscolhida(){
		Carta c = this.escolhida;
		this.escolhida = null;
		return c;
	}

	public int escolherAtributo(){
		return this.atributoEscolhido;
	}
	public void definirAtributo(int i){
		this.atributoEscolhido = i;
	}

	
}