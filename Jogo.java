/*
Nesta classe que as entradas e saidas para o console são
acopladas no supertrunfo.

Também é aqui que são feitas as limitações de opções dentro do jogo.
*/
public class Jogo{
	
	private Computador computador;
	private Jogador jogador;
	private SaidaPeloConsole saida;
	private EntradaPeloConsole entrada;
	private Baralho baralho;
	private SuperTrunfo supertrunfo;
	
	public Jogo(){
		this.saida = new SaidaPeloConsole();
		this.entrada = new EntradaPeloConsole();
		this.jogador = new Jogador();
	}

	public void iniciar(){
		supertrunfo = new SuperTrunfo(this.jogador, this.computador, this.baralho);
		supertrunfo.preparar();
	}

	public void usarBaralhoDeuses(){
		this.baralho = Baralhos.criarBaralhoDeuses();
	}

	public void rodar(){
		while(!this.supertrunfo.jogoAcabou()){
				
				this.supertrunfo.preTurno();

				if(this.supertrunfo.vezDojogador()){
					//imprime a carta do jogador.
					this.saida.imprimirCarta(this.jogador.verCartaEscolhida(), this.baralho);
					//imprime as opcoes para o jogador.
					this.saida.imprimirOpcoes(this.baralho);
					//pega a escolha no console.
					int escolhaJogador = this.entrada.getOpcao();
					//colocar a escolha do console no jogador.
					this.jogador.definirAtributo(escolhaJogador);
				}
				//roda o turno e retorna um objeto info.
				Info info = this.supertrunfo.turno();
				//imprime as informacoes do turno.
				this.saida.imprimirTurno(info);
				//finaliza o turno
				System.out.println("aperte qualquer teclha para continuar");
				this.entrada.esperarUmaTeclaQualquer();
		}

		String ganhador = this.supertrunfo.ganhadorDoJogo();
		System.out.println(ganhador+" ganhou o jogo!");
	}

	public void usarComputadorFacil(){
		this.computador = new Computador(Computador.FACIL);
	}
	public void usarComputadorMedio(){
		this.computador = new Computador(Computador.MEDIO);
	}

}