public class SaidaPeloConsole{
	
	public SaidaPeloConsole(){}

	public void imprimirTurno(Info info){

		System.out.println("------------------------------");
		this.imprimirQuemComeca(info);
		System.out.println("------------------------------");
		System.out.println("atributoEscolhido:"+info.getNomeAtributoEscolhido());		
		System.out.println("------------------------------");
		System.out.println("--JOGADOR--\t\t\t--COMPUTADOR--");
		imprimirDuasCartas(info.getCartaJogador(), info.getCartaComputador(), info.getBaralho() );
		this.imprimeGanhador(info);
		this.imprimirQuantidadeDeCartas(info);

		System.out.println();
	}

	public void imprimirDuasCartas(Carta c1, Carta c2, Baralho baralho){
		System.out.println(""+c1.getTipo()+c1.getNumero()+" nome:"+c1.getNome()+"\t\t\t"+c2.getTipo()+c2.getNumero()+" nome:"+c2.getNome());
		if(c1.ehSuperTrunfo()){
			System.out.println("supertrunfo!!!");
		}
		if(c2.ehSuperTrunfo()){
			System.out.println("\t\t\t\tsupertrunfo!!!");
		}
		String s = 
				"1) "+baralho.pegarNomeDeAtributoPorIndice(1)+" "+c1.getAtributo1()+"\t\t\t"+
				"1) "+baralho.pegarNomeDeAtributoPorIndice(1)+" "+c2.getAtributo1();
		System.out.println(s);

		s = "2) "+baralho.pegarNomeDeAtributoPorIndice(2)+" "+c1.getAtributo2()+"\t\t\t"+
			"2) "+baralho.pegarNomeDeAtributoPorIndice(2)+" "+c2.getAtributo2();
		System.out.println(s);

		s = "3) "+baralho.pegarNomeDeAtributoPorIndice(3)+" "+c1.getAtributo3()+"\t\t"+
			"3) "+baralho.pegarNomeDeAtributoPorIndice(3)+" "+c2.getAtributo3();
		System.out.println(s);

		s = "4) "+baralho.pegarNomeDeAtributoPorIndice(4)+" "+c1.getAtributo4()+"\t\t"+
			"4) "+baralho.pegarNomeDeAtributoPorIndice(4)+" "+c2.getAtributo4();
		System.out.println(s);

		s = "5) "+baralho.pegarNomeDeAtributoPorIndice(5)+" "+c1.getAtributo5()+"\t\t"+
			"5) "+baralho.pegarNomeDeAtributoPorIndice(5)+" "+c2.getAtributo5();
		System.out.println(s);
		
	}

	public void imprimirCarta(Carta carta, Baralho baralho){
		System.out.println("nome:"+carta.getNome());
		if(carta.ehSuperTrunfo()){
			System.out.println("supertrunfo!!!");
		}
		
		System.out.println("1) "+baralho.pegarNomeDeAtributoPorIndice(1)+" "+carta.getAtributo1());
		System.out.println("2) "+baralho.pegarNomeDeAtributoPorIndice(2)+" "+carta.getAtributo2());
		System.out.println("3) "+baralho.pegarNomeDeAtributoPorIndice(3)+" "+carta.getAtributo3());
		System.out.println("4) "+baralho.pegarNomeDeAtributoPorIndice(4)+" "+carta.getAtributo4());
		System.out.println("5) "+baralho.pegarNomeDeAtributoPorIndice(5)+" "+carta.getAtributo5());
		System.out.println();
	}

	public void imprimirOpcoes(Baralho baralho){
		System.out.println("escolha um atributo:");
	}

	private void imprimirQuemComeca(Info info){
		if(info.vezDoJogador()){
			System.out.println("jogador comeca!");
		}else{
			System.out.println("computador comeca!");
		}
	}

	private void imprimirQuantidadeDeCartas(Info info){
		System.out.print("jogador:"+info.quantidadeDeCartasJogador()+" ");
		System.out.print("computador:"+info.quantidadeDeCartasComputador()+" ");
		System.out.print("pilha:"+info.quantidadeDeCartasNaPilha());
		System.out.println();
			
	}

	private void imprimeGanhador(Info info){
		String ganhador = info.ganhador();
		System.out.println("------------------------------");
		if(ganhador.equals("empate")){
			System.out.println("empate!");
		}else{
			System.out.println(ganhador+" ganhou!");
		}
		System.out.println("------------------------------");
			
	}
}