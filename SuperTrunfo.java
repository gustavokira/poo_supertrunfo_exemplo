import java.util.ArrayList;
import java.util.Collections;

public class SuperTrunfo{
	private Jogador jogador;
	private Computador computador;
	private ArrayList<Carta> pilha;
	private ArrayList<Carta> cartas;
	private Baralho baralho;
	private boolean vezDojogador;

	public SuperTrunfo(Jogador j, Computador c, Baralho b){
		this.jogador = j;
		this.computador = c;
		this.baralho = b;
		this.cartas = this.baralho.pegarCartas();
		this.pilha = new ArrayList<Carta>();
		this.vezDojogador = true;
	}

	//escolhe as cartas para o jogador e computador
	public void preTurno(){
		this.jogador.escolherCarta();
		this.computador.escolherCarta();
	}

	public Info turno(){
		
		Carta cartaDoJogador = this.jogador.pegarCartaEscolhida();
		Carta cartaDoComputador = this.computador.pegarCartaEscolhida();
		int escolha = -1;

		boolean vez = this.vezDojogador;

		if(vez){
			escolha = this.jogador.escolherAtributo();
		}else{
			escolha = this.computador.escolherAtributo();
		}
		
		Carta ganhadora = baralho.cartaGanhadora(cartaDoJogador, cartaDoComputador, escolha);
		
		if(ganhadora == null){
			pilha.add(cartaDoJogador);
			pilha.add(cartaDoComputador);
		}
		else{
			if(ganhadora == cartaDoJogador){
				jogador.adicionarCarta(cartaDoComputador);
				jogador.adicionarCarta(cartaDoJogador);
				while(pilha.size() > 0){
					Carta c = pilha.remove(0);
					jogador.adicionarCarta(c);
				}

				this.jogadorEscolhePrimeiro();
			}else{
				computador.adicionarCarta(cartaDoComputador);
				computador.adicionarCarta(cartaDoJogador);
				while(pilha.size() > 0){
					Carta c = pilha.remove(0);
					computador.adicionarCarta(c);
				}
				this.computadorEscolhePrimeiro();
			}
		}

		Info info = new Info(
			this.jogador,
			this.computador,
			cartaDoJogador, 
			cartaDoComputador, 
			escolha,
			this.baralho.pegarNomeDeAtributoPorIndice(escolha),
			vez,
			this.pilha.size(),
			this.baralho
		);

		if(ganhadora == null){
			info.empate();
		}
		else if(ganhadora == cartaDoJogador){
			info.jogadorGanhou();
		}
		else if(ganhadora == cartaDoComputador){
			info.computadorGanhou();
		}

		return info;
	}

	public void preparar(){

		this.embaralhar();
		this.distribuirCartas();
	}

	//embaralha as cartas dentro do jogo.
	private void embaralhar(){
		Collections.shuffle(this.cartas);
	}

	//somente usar este método depois de embaralhar!
	private void distribuirCartas(){
		for(int i = 0;i<this.cartas.size()/2;i++){
			
			Carta c1 = this.cartas.get(i);
			this.jogador.adicionarCarta(c1);
			
			Carta c2 = this.cartas.get( (this.cartas.size()/2)+i );
			this.computador.adicionarCarta(c2);
		}
	}

	public boolean jogoAcabou(){
		if(
			this.jogador.quantidadeDeCartas() == 0 || this.computador.quantidadeDeCartas() == 0
		){
			return true;
		}else{
			return false;
		}
	}

	public String ganhadorDoJogo(){
		if(this.computador.quantidadeDeCartas() == 0){
			return "Jogador";
		}
		else if(this.jogador.quantidadeDeCartas() == 0){
			return "Computador";
		}
		return null;
	}

	public boolean vezDojogador(){
		return this.vezDojogador;
	}

	private void jogadorEscolhePrimeiro(){
		this.vezDojogador = true;
	}
	private void computadorEscolhePrimeiro(){
		this.vezDojogador = false;
	}
}